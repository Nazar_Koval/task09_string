package com.koval;

import com.koval.view.View;
import static com.koval.view.View.read;

public class Main {

    public static void main(String[] args) {
        read();
        new View().interact();
    }

}