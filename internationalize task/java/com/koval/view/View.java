package com.koval.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.koval.string.utils.StringUtils;
import java.util.*;

public class View {

    private Map<String, String> options;
    private Map<String, Executable> methods;
    private static Scanner scanner = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(View.class);
    private Locale locale;
    private ResourceBundle resourceBundle;

    private void internationalize(){
        options.put("1", resourceBundle.getString("1"));
        options.put("2", resourceBundle.getString("2"));
        options.put("3", resourceBundle.getString("3"));
        options.put("4", resourceBundle.getString("4"));
        options.put("5", resourceBundle.getString("5"));
        options.put("Q", resourceBundle.getString("Q"));
    }

    public View(){
        options = new LinkedHashMap<>();
        methods = new LinkedHashMap<>();

        locale = new Locale("en");
        resourceBundle = ResourceBundle.getBundle("Menu",locale);

        internationalize();

        methods.put("1",this::test);
        methods.put("2",this::germanLocale);
        methods.put("3",this::japaneseLocale);
        methods.put("4", this::ukrainianLocale);
        methods.put("5",this::englishLocale);
        methods.put("Q",this::exit);
    }

    private void test(){
        logger.trace(new StringUtils().getResult() + "\n");
    }

    private void germanLocale(){
        locale = new Locale("de");
        resourceBundle = ResourceBundle.getBundle("Menu",locale);
        internationalize();
    }

    private void japaneseLocale(){
        locale = new Locale("ja");
        resourceBundle = ResourceBundle.getBundle("Menu",locale);
        internationalize();
    }

    private void ukrainianLocale(){
        locale = new Locale("uk");
        resourceBundle = ResourceBundle.getBundle("Menu",locale);
        internationalize();
    }

    private void englishLocale(){
        locale = new Locale("en");
        resourceBundle = ResourceBundle.getBundle("Menu",locale);
        internationalize();
    }
    private void exit(){
        logger.trace("Bye!");
        System.exit(0);
    }

    private void show(){
        logger.trace("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
        for (String s:options.values()) {
            logger.trace(s + "\n");
        }
        logger.trace("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
        logger.trace("---> ");
    }

    public void interact(){
        String key;

        do{
            show();
            internationalize();
            key = scanner.nextLine().toUpperCase();
            methods.get(key).execute();

        }while (!key.equals("Q"));

    }
}
