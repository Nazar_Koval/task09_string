package com.koval.string.utils;

import java.util.Arrays;
import java.util.Random;

public class StringUtils {

    public static final String ALPHABET = "abcdefghijklmnopqrstuwzvx";
    private String result;

    public StringUtils(){
        execute();
    }

    private void execute(){

        StringBuilder stringBuilder = new StringBuilder();
        Random rd = new Random();

        int stringLen = rd.nextInt(16);
        int arrayLen = rd.nextInt(4);

        int numberOfIntegers = rd.nextInt(6);
        int numberOfStrings = rd.nextInt(11);
        int numberOfObjects = rd.nextInt(4);
        int numberOfArrays = rd.nextInt(4);
        int numberOfChars = rd.nextInt(16);


        for (int i = 0; i < numberOfIntegers ; i++) {
            stringBuilder.append(Integer.valueOf(rd.nextInt(1000)));
        }

        for (int i = 0; i < numberOfObjects; i++) {
            stringBuilder.append(new Object().hashCode());
        }

        for (int i = 0; i < numberOfChars; i++) {
            stringBuilder.append(ALPHABET.charAt(rd.nextInt(ALPHABET.length())));
        }

        for (int i = 0; i < numberOfStrings; i++) {
            StringBuilder temporaryBuilder = new StringBuilder();
            for (int j = 0; j < stringLen ; j++) {
                temporaryBuilder.append(ALPHABET.charAt(rd.nextInt(ALPHABET.length())));
            }
            String temporaryString = temporaryBuilder.toString();
            stringBuilder.append(temporaryString);
        }

        for (int i = 0; i <numberOfArrays ; i++) {
            int[]arr = new int[arrayLen];
            for (int j = 0; j <arrayLen ; j++) {
                arr[j] = rd.nextInt(101);
            }
            stringBuilder.append(Arrays.toString(arr));
        }

        this.result = stringBuilder.toString();
    }

    public String getResult(){
        return this.result;
    }
}

