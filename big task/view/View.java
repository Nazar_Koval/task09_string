package com.koval.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class View {

    private static Scanner scanner = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(View.class);
    private Map<String, String> options;
    private static List<String>strings = new ArrayList<>();
    private static Random rd;
    public static final String ALPHABET = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";

    public View(){
        options = new LinkedHashMap<>();
        options.put("1", "1 - Task # 1");
        options.put("2", "2 - Task # 2");
        options.put("3", "3 - Task # 3");
        options.put("4", "4 - Task # 4");
        options.put("5", "5 - Task # 5");
        options.put("6", "6 - Task # 6");
        options.put("7", "7 - Task # 7");
        options.put("8", "8 - Task # 8");
        options.put("9", "9 - Task # 9");
        options.put("10", "10 - Task # 10");
        options.put("11", "11 - Task # 11");
        options.put("12", "12 - Task # 12");
        options.put("13", "13 - Task # 13");
        options.put("14", "14 - Task # 14");
        options.put("15", "15 - Task # 15");
        options.put("16", "16 - Task # 16");
        options.put("17", "17 - Exit");
    }

    public static void read(){
        try(FileReader fileReader = new FileReader("E:\\task09\\src\\main\\java\\data.txt")){
            int c;
            StringBuilder tmp = new StringBuilder();
            String str;

            while((c = fileReader.read())!=-1){

                if((char)c == '\r'){
                    str = tmp.toString();

                    if(!str.isEmpty()) {
                        strings.add(str);
                    }

                    tmp = new StringBuilder();
                }else {

                    if((char)c != '\n')
                        tmp.append((char) c);
                }

            }
            str = tmp.toString().replaceAll("\\s+"," ");

            if(!str.isEmpty()) {
                strings.add(str);
            }

        }catch (IOException e){
            e.printStackTrace();
        }
        logger.trace("|||Text from file|||\n");
        logger.trace("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
        for (String s: strings) {
            logger.trace(s + "\n");
        }
        logger.trace("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
    }

    private static List<String>sentencesToWords(){
        List<String>words = new ArrayList<>();

        for (String s:strings) {

            if(s.contains(" ")){
                String[]s_arr = s.split(" ");

                for (String s1 : s_arr) {

                    if (!s1.equals("?"))
                        words.add(s1);

                }

            }else {

                if(!s.equals("?"))
                    words.add(s);
            }
        }
        return words;
    }

    private static int longestPalSubstr(String str)
    {
        int n = str.length();

        boolean[][] table = new boolean[n][n];

        int maxLength = 1;

        for (int i = 0; i < n; ++i)
            table[i][i] = true;

        for (int i = 0; i < n-1; ++i)
        {
            if (str.charAt(i) == str.charAt(i+1))
            {
                table[i][i+1] = true;
                maxLength = 2;
            }
        }

        for (int k = 3; k <= n; ++k)
        {
            for (int i = 0; i < n-k+1 ; ++i)
            {
                int j = i + k - 1;

                if (table[i+1][j-1] && str.charAt(i) == str.charAt(j))
                {
                    table[i][j] = true;

                    if (k > maxLength)
                    {
                        maxLength = k;
                    }
                }
            }
        }

        return maxLength;
    }

    enum Menu implements Executable{
        FIRST{
            @Override
            public void execute(){
                logger.trace("~~~~~~~TASK 1~~~~~~~\n");
                List<String>copy = strings;
                List<String>words = sentencesToWords();
                int[]ans = new int[words.size()];
                Arrays.fill(ans,0);

                for (String str:copy) {

                    for (int i = 0; i <words.size() ; i++) {

                        if(str.contains(words.get(i))){
                            ans[i] += 1;
                        }
                    }
                }
                logger.trace("Maximum number of sentences with common word: " + Arrays
                        .stream(ans)
                        .max()
                        .getAsInt() + "\n");
                logger.trace("~~~~~~~TASK 1~~~~~~~\n");
            }
        },
        SECOND{
            @Override
            public void execute() {
                logger.trace("~~~~~~~TASK 2~~~~~~~\n");
                TreeMap<Integer,String>treeMap = new TreeMap<>();

                for (String string1 : strings) {
                    treeMap.put(string1.length(), string1);
                }

                logger.trace("Result:\n");
                for (String s:treeMap.values()) {
                    logger.trace(s + "\n");
                }
                logger.trace("~~~~~~~TASK 2~~~~~~~\n");
            }
        },
        THIRD{
            @Override
            public void execute() {
                logger.trace("~~~~~~~TASK 3~~~~~~~\n");
                String[]first = strings.get(0).split(" ");
                List<String>sentences = strings;
                boolean check;
                String result = "";

                for (String s : first) {
                    if(s.equals("?"))continue;
                    check = false;

                    for (int j = 1; j < sentences.size(); j++) {

                        if (sentences.get(j).contains(s)) {
                            check = true;
                            break;
                        }

                    }

                    if (!check) {
                        result = s;
                        break;
                    }

                }

                logger.trace("Unique word: " + result + "\n");
                logger.trace("~~~~~~~TASK 3~~~~~~~\n");
            }
        },
        FOURTH{
            @Override
            public void execute() {
                logger.trace("~~~~~~~TASK 4~~~~~~~\n");
                String regex = "^.*[?]$";
                rd = new Random();
                Set<String>quest = new HashSet<>();
                int len = rd.nextInt(11);
                logger.trace("Searched length : " + len + "\n");

                for (String string : strings) {

                    if (string.matches(regex)) {

                        if (string.contains(" ")) {
                            String[] arr = string.split(" ");

                            for (String s : arr) {

                                if (s.length() == len) {
                                    quest.add(s);
                                }

                            }

                        } else {

                            if (string.length() == len) {
                                quest.add(string);
                            }

                        }

                    }
                }
                if(quest.isEmpty())
                    logger.trace("No words found");
                else {
                    logger.trace("All words found :\n");
                    logger.trace(quest);
                }
                logger.trace("\n");
                logger.trace("~~~~~~~TASK 4~~~~~~~\n");
            }
        },
        FIFTH{
            @Override
            public void execute() {
                logger.trace("~~~~~~~TASK 5~~~~~~\n");
                List<String>stringList = strings;
                String rgx = "^[AEIOUaeiou].*$";
                String maxStr;
                String firstVowel = "";
                int maxLen;

                for (int i = 0; i < stringList.size(); i++) {

                    if (stringList.get(i).contains(" ")) {
                        String[] array = stringList.get(i).split(" ");
                        maxLen = array[0].length();
                        maxStr = array[0];

                        for (int j = 1; j < array.length; j++) {

                            if (array[j].length() > maxLen) {
                                maxLen = array[j].length();
                                maxStr = array[j];
                            }

                        }

                        for (String s : array) {

                            if (s.matches(rgx)) {
                                firstVowel = s;
                                break;
                            }

                        }

                        if (!firstVowel.isEmpty()) {
                            boolean checker = false;
                            for (int k = 0; k < array.length && !checker; k++) {

                                for (int j = 0; j < array.length; j++) {

                                    if (k == j) continue;

                                    if (array[k].equals(firstVowel) && array[j].equals(maxStr)) {
                                        String tmp = array[k];
                                        array[k] = array[j];
                                        array[j] = tmp;
                                        checker = true;
                                        break;
                                    }

                                }

                            }
                            if (checker) {
                                stringList.set(i,Arrays.toString(array));
                            }

                        }

                    }

                }
                logger.trace("Result :\n");
                for (String s: stringList) {
                    logger.trace(s + "\n");
                }
                logger.trace("~~~~~~~TASK 5~~~~~~\n");
            }
        },
        SIXTH{
            @Override
            public void execute() {
                logger.trace("~~~~~~~TASK 6~~~~~~\n");
                List<String>words = sentencesToWords();
                Stream<String> stringStream = words.stream().sorted();
                words = stringStream.collect(Collectors.toList());

                for (int i = 0; i <words.size()-1 ; i++) {

                    if(words.get(i).charAt(0) == words.get(i+1).charAt(0)){
                        logger.trace(words.get(i) + " | ");
                    }else {
                        logger.trace(words.get(i) + " ");
                        logger.trace("\n");
                    }
                }
                logger.trace("\n");
                logger.trace("~~~~~~~TASK 6~~~~~~\n");
            }
        },
        SEVENTH{
            @Override
            public void execute() {
                logger.trace("~~~~~~~TASK 7~~~~~~~\n");
                String rgx = "AEIOUaeiou";
                List<String>words = sentencesToWords();
                Map<String,Integer>cMap = new HashMap<>();
                int c;

                for (String s:words) {
                    c = 0;
                    for (int i = 0; i <s.length() ; i++) {

                        if(rgx.indexOf(s.charAt(i)) != -1){
                            c++;
                        }

                    }
                    cMap.put(s,c);
                }
                cMap.entrySet().stream()
                        .sorted(Map.Entry.comparingByValue())
                        .forEach(System.out::println);
                logger.trace("~~~~~~~TASK 7~~~~~~~\n");
            }
        },
        EIGHTH{
            @Override
            public void execute() {
                logger.trace("~~~~~~~TASK 8~~~~~~~\n");
                String rgx = "^[AEIOUaeiou].*$";
                String vowels = "AEIOUaeiou";
                List<String>words = sentencesToWords();
                Map<String,Character>cMap = new HashMap<>();
                for (String word:words) {

                    if(word.matches(rgx)){

                        for (int i = 0; i <word.length() ; i++) {

                            if(vowels.indexOf(word.charAt(i)) == -1
                                    && ALPHABET.indexOf(word.charAt(i)) != -1){
                                cMap.put(word,word.charAt(i));
                                break;
                            }

                        }

                    }

                }
                cMap.entrySet().stream()
                        .sorted(Map.Entry.comparingByValue())
                        .forEach(System.out::println);
                logger.trace("~~~~~~~TASK 8~~~~~~~\n");
            }
        },
        NINTH{
            @Override
            public void execute() {
                logger.trace("~~~~~~~TASK 9~~~~~~~\n");
                rd = new Random();
                List<String>words = sentencesToWords();
                char character = ALPHABET.charAt(rd.nextInt(ALPHABET.length()));
                logger.trace("Random character: " + character + "\n");

                int count;
                Map<String,Integer>cMap = new HashMap<>();

                for (int i = 0; i <words.size() ; i++) {
                    count = 0;

                    for (int j = 0; j <words.get(i).length() ; j++) {

                        if(words.get(i).charAt(j) == character){
                            count++;
                        }

                    }
                    cMap.put(words.get(i),count);
                }
                cMap.entrySet()
                        .stream()
                        .sorted(Map.Entry.comparingByKey())
                        .sorted(Map.Entry.comparingByValue())
                        .forEach(System.out::println);
                logger.trace("~~~~~~~TASK 9~~~~~~~\n");
            }
        },
        TENTH{
            @Override
            public void execute() {
                logger.trace("~~~~~~~TASK 10~~~~~~~\n");
                String[]given = {"acde", "asgrgrgergerg", "segwsrgerbguhrg", "ef", "ggfggf",
                        "abd", "aeg", "eeggerg", "a", "ge", "?"};
                List<String> words = new ArrayList<>(Arrays.asList(given));
                Map<String,Integer>cMap = new HashMap<>();
                logger.trace("Generated words: ");
                words.forEach(s -> logger.trace(s+"|"));
                logger.trace("\n\n");
                int c;

                for (String word:words) {
                    c = 0;
                    for (String sentence: strings) {

                        if(sentence.contains(" ")){
                            String[]arr = sentence.split(" ");

                            for (String s : arr) {

                                if (word.equals(s)) {
                                    c++;
                                }

                            }
                        }else {

                            if (sentence.equals(word)){
                                c++;
                            }

                        }

                    }
                    cMap.put(word,c);
                }
                logger.trace("Result:\n");
                cMap.entrySet().stream()
                        .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                        .forEach(System.out::println);
                logger.trace("~~~~~~~TASK 10~~~~~~~\n");
            }
        },
        ELEVENTH{
            @Override
            public void execute() {
                logger.trace("~~~~~~~TASK 11~~~~~~~\n");
                logger.trace("Enter first symbol: ");
                scanner.nextLine();
                String first = scanner.nextLine();
                logger.info("Enter second symbol: ");
                String second = scanner.nextLine();
                List<String>copy = strings;

                for (String sentence : copy) {

                    int minSymbol = sentence.length();
                    int maxSymbol = -1;

                    for (int i = 0; i < sentence.length(); i++) {

                        if (String.valueOf(sentence.charAt(i))
                                .equals(first) && i < minSymbol) {
                            minSymbol = i + 1;
                        }

                        if (String.valueOf(sentence.charAt(i))
                                .equals(second) && i > maxSymbol) {
                            maxSymbol = i + 1;
                        }
                    }

                    if (maxSymbol - minSymbol >= 0) {
                        sentence = sentence.replaceAll(sentence
                                .substring(minSymbol, maxSymbol+1), "");
                    }
                    logger.trace(sentence + "\n");
                }
                logger.trace("\n");
                logger.trace("~~~~~~~TASK 11~~~~~~~\n");
            }
        },
        TWELFTH{
            @Override
            public void execute() {
                logger.trace("~~~~~~~TASK 12~~~~~~~\n");
                String rgx = "^[^AEIOUaeiou].*$";
                rd = new Random();
                int len = rd.nextInt(8);
                List<String>words = sentencesToWords();
                Map<String,Boolean>used = new HashMap<>();
                Set<String>removed = new HashSet<>();
                logger.trace("Random length: " + len + "\n");
                logger.trace("Before removal:\n");
                words.forEach(s -> logger.trace(s+" "));

                for (String word:words) {

                    if(word.matches(rgx) && word.length() == len){
                        used.put(word,true);
                    }

                }

                for (String s:used.keySet()) {

                    if(used.get(s)){
                        removed.add(s);
                        words.remove(s);
                    }

                }
                logger.trace("\nAfter removal:\n");
                words.forEach(s -> logger.trace(s+" "));
                logger.trace("\nRemoved words: " + removed + "\n");
                logger.trace("~~~~~~~TASK 12~~~~~~~\n");
            }
        },
        THIRTEENTH{
            @Override
            public void execute() {
                logger.trace("~~~~~~~TASK 13~~~~~~~\n");
                rd = new Random();
                char random = ALPHABET.charAt(rd.nextInt(ALPHABET.length()));
                List<String>words = sentencesToWords();
                Map<String,Integer>cMap = new HashMap<>();
                int c;
                logger.trace("Random character: " + random + "\n");

                for (String word:words) {
                    c = 0;
                    for (int i = 0; i <word.length() ; i++) {
                        if(word.charAt(i) == random)
                            c++;
                    }
                    cMap.put(word,c);
                }
                logger.trace("Result:\n");
                cMap.entrySet()
                        .stream()
                        .sorted(Map.Entry.comparingByKey())
                        .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                        .forEach(stringIntegerEntry -> logger.trace(stringIntegerEntry+ "\n") );
                logger.trace("~~~~~~~TASK 13~~~~~~~\n");
            }
        },
        FOURTEENTH{
            @Override
            public void execute() {
                logger.trace("~~~~~~~TASK 14~~~~~~~\n");
                List<String>words = sentencesToWords();
                int maxPal = -1;
                String wordWithPal = "";

                for(String word:words){

                    if(longestPalSubstr(word) > maxPal) {
                        maxPal = longestPalSubstr(word);
                        wordWithPal = word;
                    }

                }
                logger.trace("Lps length: " + maxPal + "\n");
                logger.trace("Found in " + wordWithPal + "\n");
                logger.trace("~~~~~~~TASK 14~~~~~~~\n");
            }
        },
        FIFTEENTH{
            @Override
            public void execute() {
                logger.trace("~~~~~~~TASK 15~~~~~~~\n");
                List<String>words = sentencesToWords();
                String first;
                List<String>newWords= new ArrayList<>();
                logger.trace("|First character sub-task|\n");

                for (String word:words) {
                    first = String.valueOf(word.charAt(0));
                    word = word.replace(word,first + word.replaceAll(first,""));
                    newWords.add(word);
                }
                logger.trace(newWords);

                logger.trace("\n|Last character sub-task|\n");
                words = sentencesToWords();
                String last;
                newWords = new ArrayList<>();
                for (String word:words) {
                    last = String.valueOf(word.charAt(word.length()-1));
                    word = word.replace(word, word.replaceAll(last,"") + last);
                    newWords.add(word);
                }
                logger.trace(newWords);

                logger.trace("~~~~~~~TASK 15~~~~~~~\n");
            }
        },
        SIXTEENTH{
            @Override
            public void execute() {
                logger.trace("~~~~~~~TASK 16~~~~~~~\n");
                rd = new Random();
                int number = rd.nextInt(strings.size());
                int len = rd.nextInt(10);
                String chosen = strings.get(number);
                String sub = "##########";
                boolean check = false;
                logger.trace("Randomly chosen  sentence: << " + chosen + " >>\n");
                logger.trace("Searched length: " + len + "\n");

                if(chosen.contains(" ")){
                    String[]ar = chosen.split(" ");

                    for (int i = 0; i <ar.length ; i++) {

                        if(ar[i].equals("?"))continue;

                        if(ar[i].length() == len){
                            ar[i] = sub;
                            check = true;
                        }

                    }
                    if(check) {
                        chosen = Arrays.toString(ar);
                        logger.trace(chosen + "\n");
                    }

                }else {

                    if(chosen.length() == len) {
                        chosen = sub;
                        check = true;
                        logger.trace(chosen + "\n");
                    }

                }
                if(!check){
                    logger.trace("No words found \n");
                }
                logger.trace("~~~~~~~TASK 16~~~~~~~\n");
            }
        },
        EXIT{
            @Override
            public void execute() {
                logger.trace("Bye!");
                System.exit(0);
            }
        }
    }

    private void show(){
        logger.trace("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
        for (String s:options.values()) {
            logger.trace(s + "\n");
        }
        logger.trace("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
        logger.trace("---> ");
    }

    public void interact(){
        int key;
        do {

            show();
            key = scanner.nextInt();
            if(key <= 0 || k > 17){
                logger.trace("Wrong option input!");
            }else{
                Menu.values()[key-1].execute();
            }
        }while (key != 17);
    }
}
