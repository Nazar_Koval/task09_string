package com.koval;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Arrays;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    private static Logger logger = LogManager.getLogger(Main.class);
    public static final String ALPHABET = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";

    public static void main(String[] args) {

        //Sentence check
        String regex = "^[A-Z].*[a-z]$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher("Hello my name is Johny");

        if(matcher.matches())
            logger.trace("Matches!\n");
        else logger.trace("Mismatches(\n");

        matcher = pattern.matcher("Hello my name is JOHNY");
        if(matcher.matches())
            logger.trace("Matches!\n");
        else logger.trace("Mismatches(\n");

        matcher = pattern.matcher("hello my name is Johny");
        if(matcher.matches())
            logger.trace("Matches!\n");
        else logger.trace("Mismatches(\n");

        matcher = pattern.matcher("HELLO MY NAME IS JOHNY");
        if(matcher.matches())
            logger.trace("Matches!\n");
        else logger.trace("Mismatches(\n");

        //Replace all vowels
        String nRegex = "[AEIOUaeiou]";
        Random rd = new Random();
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < rd.nextInt(21) ; i++) {
            stringBuilder.append(ALPHABET.charAt(rd.nextInt(ALPHABET.length())));
        }

        String test = stringBuilder.toString();
        logger.trace(test + "\n");
        String result = test.replaceAll(nRegex,"#");
        logger.trace(result + "\n");

        //Split
        String sRegex = "[t][h][e]|[y][o][u]";
        String str = "Maytheforcebewithyou";
        String[]res = str.split(sRegex);
        logger.trace(Arrays.toString(res));
    }

}
